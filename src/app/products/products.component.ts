import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Meta, Title} from '@angular/platform-browser';

interface ProductData {
  id: number;
}

@Component({
  selector: 'app-product',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  id: number = 0;
  loading = true;

  constructor (private route: ActivatedRoute, private meta: Meta, private title: Title) {
  }

  ngOnInit(): void {
    this.route.url.subscribe(async (url) => {
      console.log('url change:', this.route.pathFromRoot.reduce((url, segment) => `${url}/${segment.snapshot.url[0]?.path || ''}`.replace(/^\/+/g,''), ''));
    })
    this.route.paramMap.subscribe(params => {
      const routeId: number = +Number(params.get('id')) ?? 0;
      if (this.id === routeId) {
        this.loading = false;
      }
      this.loading = true;
      this.initData(routeId);
    });
  }

  setTitle(newTitle: string) {
    this.title.setTitle(newTitle);
  }

  async initData(id: number) {
    this.id = id;
    this.setTitle('Product' + this.id);
    const res: ProductData = await this.loadData(id);
    if (this.id !== id) {
      return;
    }
    ['description', 'author', 'keywords'].forEach((tag) => this.meta.removeTag(`name="${tag}"`));
    this.meta.addTags([
      {name: 'description', content: 'Description of products' + this.id},
      {name: 'author', content: 'product' + this.id},
      {name: 'keywords', content: 'Angular, ButterCMS, Product' + this.id}
    ]);
    this.loading = false;
  }

  loadData(id: number): Promise<ProductData> {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve({
        id,
      }), 1000);
    });
  }
}
